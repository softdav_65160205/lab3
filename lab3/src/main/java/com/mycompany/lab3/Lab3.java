package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
/**
 *
 * @author informatics
 */
public class Lab3 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    public static boolean checkWin(String[][] table, String Player) {
        if (checkRow(table, Player)) {
            return true;
        }
        if (checkCol(table, Player)) {
            return true;
        }
        if (checkDiagonal(table, Player)) {
            return true;
        }
        return false;
    }
    public static boolean swicthPlayer(String Player) {
        if(Player == "X"){
            Player = "O";
            return true;
        }else if(Player == "O"){
            Player = "X";
            return true;
        }
        return false;
    }
    
    public static boolean checkDraw(String[][] table) {
    for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == "-") {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean checkRow(String[][] table, String Player) {
        for (int col = 0; col < 3; col++) {
            if (table[0][col].equals(Player)) {
                return true;
            }
            if (table[1][col].equals(Player)) {
                return true;
            }
            if (table[2][col].equals(Player)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String Player) {
        for (int row = 0; row < 3; row++) {
            if (table[row][0].equals(Player)) {
                return true;
            }
            if (table[row][1].equals(Player)) {
                return true;
            }
            if (table[row][2].equals(Player)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiagonal(String[][] table, String Player) {
        for (int i = 0; i < 3; i++) {
            if (i == i) {
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (i == 2 - i) {
                return true;
            }
        }
        return false;
    }
}
