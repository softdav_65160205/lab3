package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class CalculationUnitTest {

    private String[][] table;

    public CalculationUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testcheckRow1_O_Verticell_output_true() {
        String[][] table = {{"O", "O", "O"},{"-","-","-"},{"-","-","-"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckRow2_O_Verticell_output_true() {
        String[][] table = {{"-","-","-"},{"O", "O", "O"},{"-","-","-"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckRow3_O_Verticell_output_truee() {
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O", "O", "O"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckRow1_X_Verticell_output_true() {
        String[][] table = {{"X", "X", "X"},{"-","-","-"},{"-","-","-"}};
        String Player = "X";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckRow2_X_Verticell_output_true() {
        String[][] table = {{"-","-","-"},{"X", "X", "X"},{"-","-","-"}};
        String Player = "X";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckRow3_X_Verticell_output_truee() {
        String[][] table = {{"-","-","-"},{"-","-","-"},{"X", "X", "X"}};
        String Player = "X";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckCol1_O_Verticell_output_true() {
        String[][] table = {{"O", "-", "-"},{"O","-","-"},{ "O","-","-"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckCol2_O_Verticell_output_true() {
        String[][] table = {{"-", "O", "-"},{"-","O","-"},{ "-","O","-"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
     public void testcheckCol3_O_Verticell_output_true() {
        String[][] table = {{"-", "-", "O"},{"-","-","O"},{ "-","-","O"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
     @Test
    public void testcheckCol1_X_Verticell_output_true() {
        String[][] table = {{"X", "-", "-"},{"X","-","-"},{ "X","-","-"}};
        String Player = "X";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckCol2_X_Verticell_output_true() {
        String[][] table = {{"-", "XO", "-"},{"-","X","-"},{ "-","X","-"}};
        String Player = "X";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
     public void testcheckCol3_X_Verticell_output_true() {
        String[][] table = {{"-", "-", "X"},{"-","-","X"},{ "-","-","X"}};
        String Player = "X";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckDiagonal1_O_Verticell_output_true() {
        String[][] table = {{"O", "-", "-"},{"-","O","-"},{ "-","-","O"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckDiagonal2_O_Verticell_output_true() {
        String[][] table = {{"-", "-", "O"},{"-","O","-"},{ "O","-","-"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckDiagonal1_X_Verticell_output_true() {
        String[][] table = {{"X", "-", "-"},{"-","X","-"},{ "-","-","X"}};
        String Player = "X";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckDiagonal2_X_Verticell_output_true() {
        String[][] table = {{"-", "-", "X"},{"-","X","-"},{ "X","-","-"}};
        String Player = "X";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
    @Test
    public void testwicthPlayer_O_Verticell_output_true() {
        String Player = "O";
        boolean result = Lab3.swicthPlayer(Player);
        assertEquals(true, result);
    }
    @Test
    public void testcheckDraw_O_Verticell_output_true() {
        String[][] table = {{"O", "X", "O"},{"X","X","O"},{ "O","O","X"}};
        String Player = "O";
        boolean result = Lab3.checkWin(table, Player);
        assertEquals(true, result);
    }
}
